import random

cartetab = ['2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '1♣', 'V♣', "D♣", "R♣", "A♣",'2♥', '3♥', '4♥', '5♥', '6♥', '7♥',
 '8♥', '9♥', '1♥', "V♥", "D♥", "R♥", "A♥",'2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '1♠', "V♠", "D♠", "R♠", "A♠",'2♦'
 , '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '1♦', "V♦", "D♦", "R♦", "A♦"]

#Fonction genérer cartes aléatoires
def random_value():
  return random.choice(cartetab)

class OnBoard:

#Fonction appliquer une nouvelle carte aléatoire par tirage   
  def __init__(self, carte=""):
    self.carte = random_value()
    #Enlever la carte tirée du paquet
    i = cartetab.index(self.carte)
    del cartetab[i]
    print(f'Cartes {self.carte} ')
#Fonction récupérer le premier caractère de la carte    
  def get_Char(self):
      return self.carte[0]

  def get_Color(self):
    print(self.carte[1])
    return self.carte[1]      
      
#Créer la carte 
carte1 = OnBoard()
carte2 = OnBoard()
carte3 = OnBoard()
carte4 = OnBoard()
carte5 = OnBoard()
carte6 = OnBoard()
carte7 = OnBoard()

class TuchFlop:
    #Initialisation
    def __init__(self,card1,card2,card3,card4,card5,card6,card7):

        self.card1 = carte1.get_Char()
        self.card2 = carte2.get_Char()
        self.card3 = carte3.get_Char()
        self.card4 = carte4.get_Char()
        self.card5 = carte5.get_Char()
        self.card6 = carte6.get_Char()
        self.card7 = carte7.get_Char()
        
    #Detecter une paire en main   
    def get_Paire_en_main(self,list=['','','','','']): 
        self.list = [self.card3, self.card4, self.card5,self.card6,self.card7]

        if (self.card1 == self.card2):
            print('Paire EN MAIN')
            if(self.card1 == self.list):
             print('BRELAN')

    #Detecter une paire au flop
    def get_Paire(self,list=['','','','','']):
        x = False
        y = False 
        b = False
        self.list = [self.card3, self.card4, self.card5,self.card6,self.card7]

        for flop in self.list:
            if self.card1 == flop:
             print('PAIRE',self.card1)
             x += 1
             b += 1
             #Detecter Double paire
             if b>1:
                 print('DOUBLE PAIRE')
             #Detecter Brelan
             if (x>1):
                 print('BRELAN')
            if self.card2 == flop:
             print('PAIRE',self.card2)
             y += 1
             b += 1
             if b>1:
                 print('DOUBLE PAIRE')
             if (y>1):
                 print('BRELAN')

                                          
pairHand = TuchFlop('','','','','','','')
pair = TuchFlop('','','','','','','')
pairHand.get_Paire_en_main() 
pair.get_Paire()
pairFLOP = TuchFlop('','','','','','','')
     
#Pour voir le paquet sans les cartes
print(cartetab)
