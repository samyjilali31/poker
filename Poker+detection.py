import random

cartetab = ['2♣', '3♣', '4♣', '5♣', '6♣', '7♣', '8♣', '9♣', '1♣', 'V♣', "D♣", "R♣", "A♣",'2♥', '3♥', '4♥', '5♥', '6♥', '7♥',
 '8♥', '9♥', '1♥', "V♥", "D♥", "R♥", "A♥",'2♠', '3♠', '4♠', '5♠', '6♠', '7♠', '8♠', '9♠', '1♠', "V♠", "D♠", "R♠", "A♠",'2♦'
 , '3♦', '4♦', '5♦', '6♦', '7♦', '8♦', '9♦', '1♦', "V♦", "D♦", "R♦", "A♦"]

#Fonction genérer cartes aléatoires
def random_value():
  return random.choice(cartetab)

class OnBoard:
#Fonction appliquer une nouvelle carte aléatoire par tirage   
  def __init__(self, carte=""):
    self.carte = random_value()
    #Enlever la carte tirée du paquet
    i = cartetab.index(self.carte)
    del cartetab[i]
    print(f'Cartes {self.carte} ')
#Fonction récupérer le premier caractère de la carte    
  def get_Char(self):
      return self.carte[0]
  def get_Color(self):
    return self.carte[1] 

carte1 = OnBoard()
carte2 = OnBoard()
carte3 = OnBoard()
carte4 = OnBoard()
carte5 = OnBoard()
carte6 = OnBoard()
carte7 = OnBoard()

suit1=['A','2','3','4','5']
suit2=['2','3','4','5','6']
suit3=['3','4','5','6','7']
suit4=['4','5','6','7','8']
suit5=['5','6','7','8','9']
suit6=['6','7','8','9','1']
suit7=['7','8','9','1','V']
suit8=['8','9','1','V','D']
suit9=['9','1','V','D','R']
suit10=['1','V','D','R','A']

liste=[carte1.get_Char(),carte2.get_Char(),carte3.get_Char(),
carte4.get_Char(),carte5.get_Char(),carte6.get_Char(),carte7.get_Char()]
#Detecter une Suite
def get_Suite(x,y):
   return frozenset(x).intersection(y)

#Detecter l'indice de la suite(frozen)  
len(get_Suite(liste,suit1))
#Affichager la suite
if len(get_Suite(liste,suit1))> 4:
    print('Suite de A à 5')
if len(get_Suite(liste,suit2))> 4:
    print('Suite de 2 à 6')
if len(get_Suite(liste,suit3))> 4:
    print('Suite de 3 à 7')
if len(get_Suite(liste,suit4))> 4:
    print('Suite de 4 à 8')
if len(get_Suite(liste,suit5))> 4:
    print('Suite de 5 à 9')
if len(get_Suite(liste,suit6))> 4:
    print('Suite de 6 à 10')
if len(get_Suite(liste,suit7))> 4:
    print('Suite de 7 à V')
if len(get_Suite(liste,suit8))> 4:
    print('Suite de 8 à D')
if len(get_Suite(liste,suit9))> 4:
    print('Suite de 9 à R')
if len(get_Suite(liste,suit10))> 4:
    print('Quinte Royale')                                    
       
class TuchFlop:
    #Initialisation
    def __init__(self,card1,card2,card3,card4,card5,card6,card7):

        self.card1 = carte1.get_Char()
        self.card2 = carte2.get_Char()
        self.card3 = carte3.get_Char()
        self.card4 = carte4.get_Char()
        self.card5 = carte5.get_Char()
        self.card6 = carte6.get_Char()
        self.card7 = carte7.get_Char()

    #Detecter une paire en main   
    def get_Paire_en_main(self,list=['','','','','']): 
        self.list = [self.card3, self.card4, self.card5,self.card6,self.card7]
        
        if (self.card1 == self.card2):
            print('\033[34mPaire EN MAIN\033[0m')
            for paire in self.list:
            
             if(self.card1 == paire):
              print('\033[37mBRELAN\033[0m')
              
    #Detecter une paire au flop
    def get_Paire(self,list=['','','','','']):
        x = False
        y = False 
        b = False
        p = False
        self.list = [self.card3, self.card4, self.card5,self.card6,self.card7]

        for flop in self.list:
            if self.card1 == flop:
             print('\033[35mPAIRE\033[0m',self.card1)
             x += 1
             b += 1
             #Detecter Double paire
             if b>1:
                 print('\033[32mDOUBLE PAIRE\033[0m')
                 p+=1
             #Detecter Brelan
             if (x>1):
                 print('\033[31mBRELAN\033[0m')
            if self.card2 == flop:
             print('\033[34mPAIRE\033[0m',self.card2)
             y += 1
             b += 1
             if b>1:
                 print('\033[32mDOUBLE PAIRE\033[0m')
                 p+=1
             if p >1:
                 print('\033[33mFULL\033[0m')
             if (y>1):
                 print('\033[31mBRELAN\033[0m')
            
class TuchColor:
    def __init__(self,card1,card2,card3,card4,card5,card6,card7):

        self.card1 = carte1.get_Color()
        self.card2 = carte2.get_Color()
        self.card3 = carte3.get_Color()
        self.card4 = carte4.get_Color()
        self.card5 = carte5.get_Color()
        self.card6 = carte6.get_Color()
        self.card7 = carte7.get_Color()
           
    def get_Color_Hand(self,list=['','','','','']): 
        self.list = [self.card3, self.card4, self.card5,self.card6,self.card7]             
        if (self.card1 == self.card2):
            print('\033[36mCOULEUR en main\033[0m')       
    def get_Color(self,list=['','','','','','','']):
         self.list = [self.card1,self.card2,self.card3, self.card4, self.card5,self.card6,self.card7]

         c= False
         ca=False
         p= False
         t= False
         for couleur in self.list:
          if couleur == '♥':
              c += 1
              if c > 4:
                  print("\033[31mCouleur ♥\033[0m")
          if couleur == '♦':
              ca += 1
              if ca > 4:
                  print("\033[31mCouleur ♦\033[0m")    
          if couleur == '♠':
              p += 1
              if p > 4:
                  print("\033[37mCouleur ♠\033[0m")  
          if couleur == '♣':
              t += 1
              if t > 4:
                  print("\033[37mCouleur ♣\033[0m")  
                      
pairHand = TuchFlop('','','','','','','')
pair = TuchFlop('','','','','','','')
pairHand.get_Paire_en_main() 
pair.get_Paire()
pairFLOP = TuchFlop('','','','','','','')
couleur = TuchColor('','','','','','','')
couleur.get_Color_Hand()
couleur5= TuchColor('','','','','','','')
couleur5.get_Color()
suite = TuchFlop('','','','','','','')






      

  

     



